<?php

class pluginJsonLd extends Plugin
{

    public function init()
    {
        // Fields and default values for the database of this plugin
        $this->dbFields = array(
            'author' => ''
        );
    }

    public function form()
    {
        global $L;
        global $pages;
        global $users;

        $html = '<div class="alert alert-primary" role="alert">';
        $html .= $this->description();
        $html .= '</div>';


        // on which static page are the authors data
        $html .= '<div>';
        $html .= '<label>' . $L->get("author") . '</label>';
        $html .= '<select name="author">';

        $list = $users->keys();
        foreach ($list as $username) {
            $html .= '<option value="' . $username . '" ' . ($this->getValue('author') === $username ? 'selected' : '') . '>' . $username . '</option>';
        }
        $html .= '</select>';
        $html .= '<span class="tip">' . $L->g('set-author') . '</span>';
        $html .= '</div>';

        // who is our main author
        $list = $pages->getList(
            $pageNumber = 1,
            1000,
            $published = false,
            $static = true,
            $sticky = true,
            $draft = false,
            $scheduled = false
        );

        return $html;
    }

    public function siteHead()
    {
        global $WHERE_AM_I;
        global $page;

        switch ($WHERE_AM_I) {
            // The user filter by page
            case 'page':
                $ld = $this->getBlogPosting($page, true);
                break;

            // The user is in the homepage
            default:
                global $content;
                $ld = $this->getUser("Person", true);;
                break;
        }
        $ld['@context']         = "http://schema.org";
        $ld["mainEntityOfPage"] = "True";

        $html = "<script type=\"application/ld+json\">\n" . json_encode($ld) . "\n</script>";
        return $html;
    }

    /**
     * @param $item Page
     * @param $full
     * @return mixed
     */
    private function getBlogPosting($item)
    {
        global $site;

        if ($item->isStatic()) {
            $posting['@type'] = 'Article';
        } else {
            $posting['@type'] = 'BlogPosting';
        }
        $this->addIfNotEmpty($posting, 'headline', $item->title());
        $this->addIfNotEmpty($posting, 'description', $item->description());
        $this->addIfNotEmpty($posting, 'url', $item->permalink($absolute = true));
        $this->addIfNotEmpty($posting, 'datePublished', $item->date(DateTimeInterface::ISO8601));
        $image = $item->coverImage($absolute = true);
        if (empty($image)) {
            // If the page doesn't have a coverImage try to get an image from the HTML content
            $pageContent = $item->content();
            $src         = DOM::getFirstImage($pageContent);
            if ($src !== false) {
                $image = $src;
            } else {
                if (Text::isNotEmpty($this->getValue('defaultImage'))) {
                    $image = $this->getValue('defaultImage');
                }
            }
        }
        if ($image) {
            $posting['image']['@type'] = "imageObject";
            $posting['image']['url']   = $image;
        }
        $this->addIfNotEmpty($posting, 'dateModified', $item->dateModified(DateTimeInterface::ISO8601));

        $author            = $this->getUser("Person", false);
        $posting['author'] = $author;

        $publisher = $this->getUser("Organization", false);
        if ($site->logo($absolute = true)) {
            $publisher['logo']['@type'] = "imageObject";
            $publisher['logo']['url']   = $site->logo($absolute = true);
        }
        $posting['publisher'] = $publisher;
        //$this->addIfNotEmpty($posting, 'articleBody', strip_tags($item->content()));
        $this->addIfNotEmpty($posting, 'description', strip_tags($item->description()));
        if (!empty($item->tags())) {
            $posting['keywords'] = $item->tags();
        }
        return $posting;
    }

    /**
     * @param $type
     * @param $full
     * @return array
     * @throws Exception
     */
    private function getUser($type, $full)
    {
        global $site;

        $person = array();
        if (!empty($this->getValue('author'))) {
            $user = new User($this->getValue('author'));
            $this->addIfNotEmpty($person, 'name', $user->firstName() . " " . $user->lastName());
            $this->addIfNotEmpty($person, 'url', $site->url());
            $this->addIfNotEmpty($person, 'image', $user->profilePicture($absolute = true));
            $this->addIfNotEmpty($person, 'email', $user->email());

            if ($full) {
                $sameAs = array();
                $this->addIfNotEmpty($sameAs, 'codepen', $user->codepen());
                $this->addIfNotEmpty($sameAs, 'gitlab', $user->gitlab());
                $this->addIfNotEmpty($sameAs, 'github', $user->github());
                $this->addIfNotEmpty($sameAs, 'instagram', $user->instagram());
                $this->addIfNotEmpty($sameAs, 'facebook', $user->facebook());
                $this->addIfNotEmpty($sameAs, 'linkedin', $user->linkedin());
                $this->addIfNotEmpty($sameAs, 'twitter', $user->twitter());
                $this->addIfNotEmpty($sameAs, 'mastodon', $user->mastodon());
                if (!empty($sameAs)) {
                    $person['sameAs'] = array_values($sameAs);
                }
            }
        }
        if (!empty($person)) {
            $person['@type'] = $type;
        }
        return $person;
    }

    /**
     * @param $array
     * @param $field
     * @param $content
     */
    private function addIfNotEmpty(&$array, $field, $content)
    {
        if (!empty($content)) {
            $array[$field] = $content;
        }
    }

}